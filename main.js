let incomeSalary = document.getElementById('income-salary'), //Основной доход
    incomeFreelance = document.getElementById('income-freelance'), //доход с  фриланса
    incomeExtra1 = document.getElementById('income-extra-1'), //доп доход 1
    incomeExtra2 = document.getElementById('income-extra-2'), //доп доход 2

    costsFlat = document.getElementById('costs-flat'),
    costsHouseServices = document.getElementById('costs-house-services'),
    costsTransport = document.getElementById('costs-transport'),
    costsCredit = document.getElementById('costs-credit');

let inputsWrap = document.querySelectorAll('.inputs-wrapper');

let totalMonth = document.getElementById('total-month'), // тотал месяц
    totalDay = document.getElementById('total-day'), // тотал день
    totalYear = document.getElementById('total-year'); // тотал год

let spend = document.getElementById('spend'), //тратим
    accumulation = document.getElementById('accumulation'); //Копим

let inputTypeRange = document.getElementById('type-range'), // ползунок с накоплением
    totalRange = document.getElementById('totalRange'); // итог ползунка в %;



function checkInput () {
    totalMonth.value = +incomeSalary.value + +incomeFreelance.value + +incomeExtra1.value + +incomeExtra2.value - +costsFlat.value - +costsHouseServices.value - +costsTransport.value - +costsCredit.value;
    totalDay.value = Math.round(totalMonth.value / 30);
    spend.value = Math.round(totalMonth.value);
}

for (input of inputsWrap) {
    input.addEventListener('input', checkInput);
}

inputTypeRange.addEventListener('input', function () {
    totalRange.innerHTML = inputTypeRange.value + '%';
    accumulation.value = Math.round(totalMonth.value/100 * inputTypeRange.value);
    spend.value = Math.round(totalMonth.value - accumulation.value);
    totalYear.value = Math.round(accumulation.value * 12);
    totalDay.value = Math.round(spend.value / 30);
})




